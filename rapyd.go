package main

/**
Translation of python code to Golang and as per Golang standards.
Ref of code snippet: https://docs.rapyd.net/reference#code-samples
Stored as rapyd.py in the VS workspace.
*/
import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
<<<<<<< HEAD
	"encoding/json"
=======
>>>>>>> RapydPrototyping
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/sendgrid/rest"
)

/**
Keys used in postman collection and validated to work correctly
GET /v1/rates/daily?action_type=payment&buy_currency=USD&sell_currency=EUR HTTP/1.1
Content-Type: application/json
access_key: AA012978B1D89C20E855
salt: 565ab8304f3fa5eda4ae6358
timestamp: 1611071576
signature: YmFjNWY1NWFmNjE5MTlmMzQ0YmQ2MjY5ZDA5MTE3NDU4NWM1YzIyZDhiOGVkYzVmNTkxYjE4NDM5MmU1YzgxMg==
User-Agent: PostmanRuntime/7.26.8
Accept: */ /*
Postman-Token: 22e1012b-c925-4751-9e8d-27f4591a462e
Host: sandboxapi.rapyd.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
HTTP/1.1 200 OK
Date: Tue, 19 Jan 2021 15:53:07 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 300
Connection: keep-alive
Strict-Transport-Security: max-age=8640000; includeSubDomains
ETag: W/"12c-HsycDZtLE3E9CraopnlWIw"
Vary: Accept-Encoding
{"status":{"error_code":"","status":"SUCCESS","message":"","response_code":"","operation_id":"d907dde2-7471-48f2-8d38-8c9e3a390898"},"data":{"sell_currency":"EUR","buy_currency":"USD","fixed_side":null,"action_type":"payment","rate":1.171576,"date":"2021-01-19","sell_amount":null,"buy_amount":null}}

*/

var accessKey = "AA012978B1D89C20E855"
var secretKey = "0f79485799459db27a176b39480b8cfd9eda4b259ef09be63fd662e80b141fd641e6ebd94e3813cf"
var baseURI = "https://sandboxapi.rapyd.net"
var path = "/v1/rates/daily?action_type=payment&buy_currency=USD&sell_currency=SGD"

/**
Generating salt .
Ensuring atleast one of them is digits to keep it sane.
Golang does not believe in magic and so not much constant lib support. Hence using the alpha numeric char set explicitly.
*/
func generateSalt(length int) string {
	rand.Seed(time.Now().UnixNano())
	digits := "0123456789"
	charSet := "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		digits
	buf := make([]byte, length)

	// atleast one integer.
	buf[0] = digits[rand.Intn(len(digits))]

	for i := 1; i < length; i++ {
		buf[i] = charSet[rand.Intn(len(charSet))]
	}
	//Shuffling
	rand.Shuffle(len(buf), func(i, j int) {
		buf[i], buf[j] = buf[j], buf[i]
	})
	str := string(buf)
	return str
}

/**
Equivalent of this.
def get_unix_time(days=0, hours=0, minutes=0, seconds=0):
	return int(time.time())
**/
func getUnixTime() int64 {
	//returns int64 hence unboxing it.
	return time.Now().Unix()
}

//signature = BASE64 ( HASH ( http_method + url_path + salt + timestamp + access_key + secret_key + body_string ) )
//HASH is the HMAC-SHA256 algorithm.
/**
def update_timestamp_salt_sig(http_method, path, body):
    if path.startswith('http'):
        path = path[path.find(f'/v1'):]
    salt = generate_salt()
    timestamp = get_unix_time()
    to_sign = (http_method, path, salt, str(timestamp), access_key, secret_key, body)

    h = hmac.new(secret_key.encode('utf-8'), ''.join(to_sign).encode('utf-8'), hashlib.sha256)
    signature = base64.urlsafe_b64encode(str.encode(h.hexdigest()))
    return salt, timestamp, signature
*/

func updateTimestampSaltSig(httpMethod string, path string, body string) (salt string, timestamp string, signature string) {
	len := 12 //max possible char length. Choosing to keep it strict.
	salt = generateSalt(len)
	timestamp = strconv.FormatInt(getUnixTime(), 10)
	toSign := strings.ToLower(httpMethod) + path + salt + timestamp + accessKey + secretKey + body
	/**
		In other languages hmac hashing with sha256 is an one-liner that has both secretKey and the message.
		However, golang is different. SecretKey is used to create the hasher and message is passed in-line to get the signature.
		[]byte in golang is precise coding format for encoding("utf-8"). Golang loves being cryptic. hex is for creating hex digest
	    Ref: https://stackoverflow.com/questions/10701874/generating-the-sha-hash-of-a-string-using-golang
	*/
	hash := hmac.New(sha256.New, []byte(secretKey))
	hash.Write([]byte(toSign))
	// Tried base64.StdEncoding too!
	signature = base64.StdEncoding.EncodeToString([]byte(hex.EncodeToString(hash.Sum(nil))))
	return salt, timestamp, signature
}

/**
def current_sig_headers(salt, timestamp, signature):
    sig_headers = {'access_key': access_key,
                   'salt': salt,
                   'timestamp': str(timestamp),
                   'signature': signature,
                   'idempotency': str(get_unix_time()) + salt}
    return sig_headers
**/

func currentSigHeaders(salt string, timestamp string, signature string) map[string]string {
	sigHeaders := map[string]string{
		"access_key":   accessKey,
		"salt":         salt,
		"timestamp":    timestamp,
		"signature":    signature,
		"Content-Type": "application/json",
	}
	return sigHeaders
}

//test for JSON string in body.
//func isJSONString(jsonStr string) bool {
// 	var validatedJSON string
// 	return json.Unmarshal([]byte(jsonStr), &validatedJSON) == nil
// }

/**
def pre_call(http_method, path, body=None):
    str_body = json.dumps(body, separators=(',', ':'), ensure_ascii=False) if body else ''
    salt, timestamp, signature = update_timestamp_salt_sig(http_method=http_method, path=path, body=str_body)
	return str_body.encode('utf-8'), salt, timestamp, signature
*/
func preCall(httpMethod string, path string, body string) ([]byte, string, string, string) {
	salt, timestamp, signature := updateTimestampSaltSig(httpMethod, path, body)
	return []byte(body), salt, timestamp, signature
}

/**
def create_headers(http_method, url,  body=None):
    body, salt, timestamp, signature = pre_call(http_method=http_method, path=url, body=body)
	return body, current_sig_headers(salt, timestamp, signature)
*/
func createHeaders(httpMethod string, path string, body string) ([]byte, map[string]string) {
	bodyInBytes, salt, timestamp, signature := preCall(httpMethod, path, body)
	return bodyInBytes, currentSigHeaders(salt, timestamp, signature)
}

/**
Returns unauthenticated call error - 401
eg:
{GET https://sandboxapi.rapyd.net/v1/rates/daily map[Content-Type:application/json access_key:AA012978B1D89C20E855 salt:JAcJaSy9thm0 signature:NDM1NGFlZDg5MGU3ZGVlNTcwOGE3MGE2NzZiODMyMGY1NzIyYzhkYjg4NjE0YjJkYzhhMTRlMmUyYmJkMjA2Ng== timestamp:1611071596] map[action_type:payment buy_currency:USD sell_currency:EUR] []}
2021/01/19 21:23:17 Invalid status code  &{401 {"status":{"error_code":"UNAUTHENTICATED_API_CALL","status":"ERROR","message":"Invalid signature","response_code":"UNAUTHENTICATED_API_CALL","operation_id":"7f3d13ae-f58a-45a4-b9d9-867831d8c988"}} map[Content-Length:[196] Content-Type:[application/json; charset=utf-8] Date:[Tue, 19 Jan 2021 15:53:17 GMT] Etag:[W/"c4-INLw+Qv45CrL6IKEVnC+tQ"] Strict-Transport-Security:[max-age=8640000; includeSubDomains] Vary:[Accept-Encoding]]} 401


request &{GET https://sandboxapi.rapyd.net/v1/rates/daily?action_type=payment&buy_currency=USD&sell_currency=SGD HTTP/1.1 1 1 map[Access_key:[AA012978B1D89C20E855] Content-Type:[application/json] Salt:[d2-kgNXi_vQ=] Signature:[NmVhNjU5NzI3YTVjN2FiODQyMDg5M2Q1ZGY1MDI1ZDIxODhlN2M3MjE0NGY3MTdjMDVlZDA3NjM0YzEwYWFhNw==] Timestamp:[1611081809]] {} 0x6bb0e0 0 [] false sandboxapi.rapyd.net map[] map[] <nil> map[]   <nil> <nil> <nil> 0xc000024060}
*/
func main() {
	fmt.Println("Rapyd List Daily Rates Call")
	fullURI := baseURI + path

	// queryParams := make(map[string]string)
	// queryParams["action_type"] = "payment"
	// queryParams["buy_currency"] = "USD"
	// queryParams["sell_currency"] = "EUR"

	method := rest.Get
	_, headers := createHeaders(string(method), path, "")

	request := rest.Request{
		Method:  method,
		BaseURL: fullURI,
		Headers: headers,
		//	QueryParams: queryParams,
	}
	_, e := rest.BuildRequestObject(request)
	if e != nil {
		fmt.Println("Error during BuildRequestObject: ", e)
	}

	fmt.Println(request)
	response, e := rest.Send(request)
	if response.StatusCode != http.StatusOK {
		fmt.Println("Invalid status code ", response, response.StatusCode)
	}
	fmt.Println(response)
}
